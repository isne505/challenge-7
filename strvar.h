﻿#ifndef STRVAR_H
#define STRVAR_H
#include <iostream>
using namespace std;
namespace strvarohm
{
	class StringVar
	{
	public:
		StringVar(int size);//size constructor
		StringVar();//default size is 100
		StringVar(const char a[]);//constructor using array
		StringVar(const StringVar& string_object);//Copy Constructor
		~StringVar();//Destructor, returns memory to heap
		int length() const;//Returns the length of the current string
		void input_line(istream& ins);
		//Precondition: if ins is a file input stream attached to a file
		//The next text up to â€˜\nâ€™, or the capacity of the stringvar is copied
		friend ostream& operator <<(ostream& outs, const StringVar& the_string);
		//Precondition: if outs is a file it has been attached to a file
		//Overloads the << operator to allow output to screen
		char one_char(int select);
		//function that return character from number position that filled
		void set_char(int select, char input);
		//function that change character from number position
		friend bool operator==(StringVar &str1, StringVar &str2);
		//function that compare 2 word from user
		friend StringVar operator+(StringVar &str1, StringVar &str2);
		//function that make 2 word linked
		void copy_piece(int select, int nextchar);
		//function that show first character that user pick to nearby character
		friend istream& operator >> (istream& outs, const StringVar& str);
		//Function that store word to variable
	
	private:
		char *value; 
		int max_length;
	};
}
#endif 