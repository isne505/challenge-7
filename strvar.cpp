#include <iostream>
#include <cstdlib>
#include <cstddef>
#include <cstring>
#include "strvar.h"

using namespace std;

namespace strvarohm
{
	StringVar::StringVar(int size) : max_length(size)//Uses cstddef and cstdlib
	{
		value = new char[max_length + 1];
		value[0] = '\0';
	}

	StringVar::StringVar() : max_length(100)//Uses cstddef and cstdlib
	{
		value = new char[max_length + 1];
		value[0] = '\0';
	}

	StringVar::StringVar(const char a[]) : max_length(strlen(a))// Uses cstring, cstddef and cstdlib
	{
		value = new char[max_length + 1];

		for (int i = 0; i<strlen(a); i++)
		{
			value[i] = a[i];
		}
		value[strlen(a)] = '\0';
	}

	StringVar::StringVar(const StringVar& string_object) : max_length(string_object.length())//Uses cstring, cstddef and cstdlib
	{
		value = new char[max_length + 1];
		for (int i = 0; i<strlen(string_object.value); i++)
		{
			value[i] = string_object.value[i];
		}
		value[strlen(string_object.value)] = '\0';
	}

	StringVar::~StringVar()
	{
		delete[] value;
	}

	
	int StringVar::length() const//Uses cstring
	{
		return strlen(value);
	}


	void StringVar::input_line(istream& ins)//Uses iostream
	{
		ins.getline(value, max_length + 1);
	}

	
	ostream& operator << (ostream& outs, const StringVar& the_string)//Uses iostream
	{
		outs << the_string.value;
		return outs;
	}


	char StringVar::one_char(int pick)
	{
		return value[pick-1];
	}


	void StringVar::set_char(int pick, char input)
	{
		value[pick - 1] = input;
	}


	bool operator==(StringVar &str1, StringVar &str2)
	{
		string firstword = str1.value;
		string secondword = str2.value;

		if (firstword == secondword)
			return true;
		else
			return false;
	}


	StringVar operator+(StringVar &str1, StringVar &str2)
	{
		StringVar word;
		for (int i = 0; i < str1.max_length + 1; i++)
		{
			word.value[i] = str1.value[i];
		}
		for (int i = str1.length(); i < str2.max_length + 1; i++)
		{
			word.value[i] = str2.value[i - str1.length()]; 
		}
		return word;
	}


	void StringVar::copy_piece(int select, int nextchar)
	{
		for (int i = select - 1; i < ((select - 1) + nextchar) + 1; i++)
		{
			cout << value[i];
		}
		cout << endl;
	}


	istream& operator >> (istream& input, const StringVar& str)
	{
		input >> str.value;
		return input;
	}
}